
README

These instructions will allow you to set-up arcade module with pnFlashGames.

1.) Download Arcade module and flashnode module
(http://drupal.org/project/flashnode) and upack in a suitable location
(e.g., sites/all/modules) and enable both modules.

2.) Go to admin/content/node-type/flashnode and under 'Arcade Settings', set
'Game?' to 'Yes'.

3.) Download games from pnFlashGames.com . When asked for 'type of website',
choose anything except 'Static HTML.' Download and unpack the file.

4.) Go to node/add/flashnode . Under 'Flash file' use the 'Browse' button to
select the .swf file contained in the paclage you just downloaded.

5.) Under Arcade settings, select 'Game?' as 'Yes'. Leave 'Path' blank. Set the
'Game Type' as appropriate for your game. Optionally enable 'Secure mode,'
although doing so may prevent some games from working, and will cause a slight
performance penalty.

6.) Save the node.

7.) Verify block placement for High Score blocks on admin/build/blocks . You
don't need to specify pages for the blocks to appear on; High Score per Game
blocks will automatically appear on flashnode pages, and User's High Score blocks
will automatically appear on user pages, for users with appropriate permissions.

8.) Grant 'view high score' permission on admin/users/permissions as desired.

9.) Enjoy your games.

10.) Send a tip via PayPal or an Amazon.com Giftcard to payments@NinjitsuWeb.com

